package index;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.TreeMap;

import index.Score;

public class InvertedIndex{
	// Array of documents  
    static String Docs[] = {"documents/christinaAguilera", "documents/mariahCarey", "documents/cher"};
    private static Scanner scanner;
    
	public InvertedIndex(){
		//stores word & frequency
		
        //printAllCounts(frequency);
    }
    
    public static int getCount(String word, TreeMap<String, Integer> frequency){
    	 //word is mentioned, frequency +1
    	if (frequency.containsKey(word)){ 
            return frequency.get(word);  
        }
        else { //no mention of that word 
            return 0;
        }
    }
    
    public static int getDocCount(String doc, TreeMap<String, Integer> documentScore){
    	if(documentScore.containsKey(doc)){
    		return documentScore.get(doc);
    	}else{
    		return 0;
    	}
    }

    public static void print(TreeMap<String, Integer> documentScore){
        for(String document : documentScore.keySet( )){
            System.out.printf("%15d    %s\n",  documentScore.get(document), document);
        }
    }

	public static void readWordFile(String query, TreeMap<String, Integer> frequencyData, TreeMap<String, Integer> documentScore){
        int total = 0;
        Scanner wordFile;
        String word;      //word read from file 
        Integer count;   //number of word counts
        int counter = 0;
        int docs = 0;
        int docScore=0;
        int queryScore=0;

        //Read document  
        for(int i=0; i<Docs.length; i++){ 
            try{
                wordFile = new Scanner(new FileReader(Docs[i]));
                
                while (wordFile.hasNext()) {
                    //next word  
                    word = wordFile.next();
                   
                    //to lower case and delete all non alphabetic 
                    word = word.toLowerCase();
                    word = word.replaceAll("[^a-zA-Z\\s]", "");
                    
                    //stop words that don't count
                    String stopWords[]={"en", "en", "het", "de", "een", "zo", "in", "op", "te", "als", "die", 
                    		"haar", "is", "met", "niet", "te", "uit", "van", "voor", "wordt", "ze"};
                    
                    //replace every stopword
                    for(int s=1; s<stopWords.length; s++){
                        if(word.equals(stopWords[s])){
                            word = word.replace(stopWords[s]+"[\\s]", ""); 
                        }
                    }
                    
                    //count+1 for this word, and store the new count value
                    count = getCount(word, frequencyData) + 1;
                    frequencyData.put(word, count);
                    total = total + count;
                    counter++;
                    docs = i + 1;
                    
                    //IR model
                    if(word.equals(query)){
                    	queryScore = getDocCount(Docs[i], documentScore) +1;
                    	docScore = queryScore/total;
                    	documentScore.put(Docs[i], queryScore);
                    }
                }
             }
            catch (FileNotFoundException e){
                System.err.println(e);
              return;
            }
        }   
        System.out.println( total + " words in the documents");
        System.out.println( counter + " single words in the documents");
        System.out.println( docs + " documents read");
       
    } 
    
    
    public static void main(String[] args) {
 		//start with the inverted index
    	scanner = new Scanner(System.in);
    	System.out.println("\nType a query below and hit enter:");
		String query = scanner.nextLine();
		
    	TreeMap<String, Integer> documentScore =  new TreeMap<String, Integer>();
    	TreeMap<String, Integer> frequency = new TreeMap<String, Integer>();
        readWordFile(query, frequency, documentScore);
        System.out.println("\ntop documents with " +query);
 	    print(documentScore);
    }
}